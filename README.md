# Racket-Lang practices

### ***You can click on the image or file to see each code*** 
---
## practice of the first series

### practice 1: 
[![1](/assets/practice-1.png)](/practice-1.rkt)

### practice 2: 
[![2](/assets/practice-2.png)](/practice-2.rkt)

### practice 3: 
[![3](/assets/practice-3.png)](/practice-3.rkt)

### practice 4: 
[![4](/assets/practice-4.png)](/practice-4.rkt)

### practice 5: 
[![5](/assets/practice-5.png)](/practice-5.rkt)

---
## practice of the second series

### practice 6: 
[![6](/assets/practice-6.png)](/practice-6.rkt)

### practice 7: 
[![7](/assets/practice-7.png)](/practice-7.rkt)

### practice 8: 
[![8](/assets/practice-8.png)](/practice-8.rkt)

### practice 9: 
[![9](/assets/practice-9.png)](/practice-9.rkt)




