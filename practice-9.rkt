#!/usr/bin/racket
#lang racket

; this sudo code!
; Define Function
(define (hanoi n source tmp destination)
  (if (= n 1)
      (println (string-append "Move disk from " source " to " destination))
      (begin
        (hanoi (- n 1) source destination tmp)
        (println (string-append "Move disk from " source " to " destination))
        (hanoi (- n 1) tmp source destination))
    )
)

; Call Function
(printf "call hanoi(\"3\" \"a\" \"b\" \"c\") is =>\n")
(hanoi 3 "a" "b" "c")
